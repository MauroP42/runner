using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_PowerUp : MonoBehaviour
{
    public static float enemyVelocity;
    private Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        //Velocidad menor a la de los enemigos
        rb.AddForce(new Vector3(-enemyVelocity / 5, 0, 0), ForceMode.Force);
        OutOfBounds();
    }

    public void OutOfBounds()
    {
        if (this.transform.position.x <= -15)
        {
            Destroy(this.gameObject);
        }
    }
}
