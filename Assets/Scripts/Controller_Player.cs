﻿using UnityEngine;

public class Controller_Player : MonoBehaviour
{
    private Rigidbody rb;
    public float jumpForce = 10;
    private float initialSize;
    private int i = 0;
    private bool floored;

    private bool shielded = false;

    public GameObject shield;
    [SerializeField] Material material1;
    [SerializeField] Material material2;

    private bool blue = true;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        initialSize = rb.transform.localScale.y;
    }

    void Update()
    {
        GetInput();

        if (shielded == true) //activa visualmente el escudo
        {
            shield.SetActive(true);
        }
        else
        {
            shield.SetActive(false);
        }
    }

    private void GetInput()
    {
        Jump();
        Duck();
        Color();
    }

    private void Jump()
    {
        if (floored)
        {
            if (Input.GetKeyDown(KeyCode.W))
            {
                rb.AddForce(new Vector3(0, jumpForce, 0), ForceMode.Impulse);
            }
        }
    }

    private void Duck()
    {
        if (floored)
        {
            if (Input.GetKey(KeyCode.S))
            {
                if (i == 0)
                {
                    rb.transform.localScale = new Vector3(rb.transform.localScale.x, rb.transform.localScale.y / 2, rb.transform.localScale.z);
                    i++;
                }
            }
            else
            {
                if (rb.transform.localScale.y != initialSize)
                {
                    rb.transform.localScale = new Vector3(rb.transform.localScale.x, initialSize, rb.transform.localScale.z);
                    i = 0;
                }
            }
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.S))
            {
                rb.AddForce(new Vector3(0, -jumpForce, 0), ForceMode.Impulse);
            }
        }
    }

    private void Color()
    {
        //Al apretar la tecla Q el jugador cambia de color
        if (Input.GetKeyDown(KeyCode.Q))
        {
            if (blue == true)
            {
                GetComponent<Renderer>().material = material2;
                blue = false;
            }
            else
            {
                GetComponent<Renderer>().material = material1;
                blue = true;
            }
        }
    }

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            if (shielded == true) //Si el jugador toca a un enemigo mientras tenga un escudo, el enemigo es destruido
            {
                shielded = false;
                Destroy(collision.gameObject);
            }
            else
            {
                Destroy(this.gameObject);
                Controller_Hud.gameOver = true;
            }
            
        }

        if (collision.gameObject.CompareTag("Floor"))
        {
            floored = true;
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("PowerUp")) //El jugador gana un escudo al tocar el power up
        {
            shielded = true;
            Destroy(other.gameObject);
        }

        if (other.gameObject.CompareTag("Blue")) //Si el jugador toca una pared que no es de su mismo color pierde, a menos que tenga un escudo
        {
            if (blue == false)
            {
                if (shielded == true)
                {
                    shielded = false;
                    Destroy(other.gameObject);
                }
                else
                {
                    Destroy(this.gameObject);
                    Controller_Hud.gameOver = true;
                }
                
            }
        }
        if (other.gameObject.CompareTag("Green"))
        {
            if (blue == true)
            {
                if(shielded == true)
                {
                    shielded = false;
                    Destroy(other.gameObject);
                }

                Destroy(this.gameObject);
                Controller_Hud.gameOver = true;
            }
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Floor"))
        {
            floored = false;
        }
    }
}
