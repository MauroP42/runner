using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Enemy5 : MonoBehaviour
{
    public static float enemyVelocity;
    private Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        //El enemigo se mueve lentamente hasta cierto punto, luego se mueve mucho mas rapido
        if(this.transform.position.x > 7)
        {
            rb.AddForce(new Vector3(-enemyVelocity / 3, 0, 0), ForceMode.Force);
        }
        else
        {
            rb.AddForce(new Vector3(-enemyVelocity * 5, 0, 0), ForceMode.Force);
        }
        OutOfBounds();
    }

    public void OutOfBounds()
    {
        if (this.transform.position.x <= -15)
        {
            Destroy(this.gameObject);
        }
    }
}
