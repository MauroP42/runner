using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Enemy2 : MonoBehaviour
{
    public static float enemyVelocityX;
    public static float enemyVelocityY;
    private Rigidbody rb;
    private bool movingUp;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        movingUp = true;
    }

    void Update()
    {
        //Detecta si el enemigo se debe mover para arriba o abajo
        if(this.transform.position.y > 1.5)
        {
            movingUp = false;
        }
        if(this.transform.position.y < -0.4)
        {
            movingUp = true;
        }

        switch (movingUp)
        {
            case true:
                rb.AddForce(new Vector3(-enemyVelocityX, enemyVelocityY*3f, 0), ForceMode.Force);
                break;
            case false:
                rb.AddForce(new Vector3(-enemyVelocityX,-enemyVelocityY*3f, 0), ForceMode.Force);
                break;
        }

        OutOfBounds();
    }

    public void OutOfBounds()
    {
        if (this.transform.position.x <= -15)
        {
            Destroy(this.gameObject);
        }
    }
}
