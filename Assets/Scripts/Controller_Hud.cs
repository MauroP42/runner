﻿using UnityEngine;
using UnityEngine.UI;

public class Controller_Hud : MonoBehaviour
{
    public static bool gameOver = false;
    public Text distanceText;
    public Text gameOverText;
    private float distanceF = 0;
    private int distanceI = 0;

    void Start()
    {
        gameOver = false;
        distanceF = 0;
        distanceText.text = distanceF.ToString();
        gameOverText.gameObject.SetActive(false);
    }

    void Update()
    {

        distanceI = (int)distanceF; //cambia la distancia a un numero entero

        if (gameOver)
        {
            Time.timeScale = 0;
            gameOverText.text = "Game Over \n Total Distance: " + distanceI.ToString();
            gameOverText.gameObject.SetActive(true);
        }
        else
        {
            distanceF += Time.deltaTime;
            distanceText.text = distanceI.ToString();
        }
    }
}
